import re

cache = {}


def lhs(cnt):
    if cnt in cache:
        return cache[cnt]
    nums = '0123456789'
    ops = '-+/*'

    prefix = [c for c in nums + ops]
    if cnt == 1:
        return [x for x in nums]  # Must end with a number
    exprs = []

    nxt = lhs(cnt - 1)

    for val in prefix:
        for n in nxt:
            if val in ops and n[0] in ops:  # rule out consecutive operators
                continue
            else:
                expr = val + n
            exprs.append(expr)

    cache[cnt] = exprs
    return cache[cnt]


def gen_dictionary():
    words = []
    lonely_zeros = re.compile(r'.*([*-+/]|\b)0[\d]*([*-/+]|\b).*')
    for i in range(1, 7):
        for e in lhs(i):
            if not e[0].isdigit():
                continue  # Not "nice", pretty sure nerdle won't use it
            if lonely_zeros.match(e):
                continue
            val = parse(e)
            if val == int(val) and val >= 0:
                word = e + '=' + str(int(val))
                if len(word) == 8:
                    words.append(word)

    for word in sorted(words):
        print(word)


def op(a, b, o):
    if o == '*':
        return a * b
    elif o == '+':
        return a + b
    elif o == '-':
        return a - b
    elif o == '/':
        return a / b


def parse(expr):
    nums = []
    ops = []
    n = 0
    for c in expr:
        if c.isdigit():
            n = n * 10 + int(c)
        else:
            if n:
                nums.append(n)
                n = 0
            ops.append(c)
    if n:
        nums.append(n)
    # Compress
    if len(ops) == 0:
        return nums[0]
    if len(ops) == 1:
        return op(nums[0], nums[1], ops[0])
    else:  # 2 ops
        if ops[1] in '*/' and ops[0] in '+-':
            return op(nums[0], op(nums[1], nums[2], ops[1]), ops[0])
        else:
            return op(op(nums[0], nums[1], ops[0]), nums[2], ops[1])


if __name__ == '__main__':
    gen_dictionary()
