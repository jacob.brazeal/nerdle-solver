import sys
from collections import defaultdict, Counter
from random import choice, sample, seed

cache = {}

GREEN = 2
PURPLE = 1
BLACK = 0

ALL_RIGHT = (2, 2, 2, 2, 2, 2, 2, 2)


def load_dictionary():
    with open('words.txt', 'r') as w:
        return [line.rstrip() for line in w]


def grade(guess, target):
    res = []

    count_by_imperfect_char = defaultdict(int)
    for (a, b) in zip(guess, target):
        if a == b:
            res.append(GREEN)
        elif a in target:
            res.append(PURPLE)
            count_by_imperfect_char[b] += 1
        else:
            res.append(BLACK)
            count_by_imperfect_char[b] += 1

    # Now clean up the bogus purples
    for (i, (a, b, c)) in enumerate(zip(guess, target, res[:])):
        if c == PURPLE:
            count_by_imperfect_char[a] -= 1
            if count_by_imperfect_char[a] < 0:
                res[i] = BLACK
    return tuple(res)


def test_grades():
    for (guess, target, pattern) in [
        ("2+2+9=13", "1+2+8=11", (0, 2, 2, 2, 0, 2, 2, 0)),
        ("44-28=16", "48-32=16", (2, 0, 2, 1, 1, 2, 2, 2)),
        ("1+1++1=3", "1++1+2=4", (2, 2, 1, 1, 2, 0, 2, 0)),
    ]:
        if (ans := grade(guess, target)) != pattern:
            print("Error: Guess=", guess, "; Target=", target, " Expected=", fmt(pattern), "Got: ", fmt(ans))


def make_grader(target):
    def grader(guess):
        return grade(guess, target)

    return grader


def fmt(grade):
    return ''.join({1: "🟪", 2: "🟩", 0: "🟫"}[c] for c in grade)


def pick_best(target_pool, guess_pool=None):
    if guess_pool is None:
        guess_pool = target_pool
    pools_by_guess = defaultdict(Counter)
    for guess in guess_pool:
        for target in target_pool:
            score = grade(guess, target)
            pools_by_guess[guess][score] += 1
    best, best_score = None, 1e9
    for guess, pools in pools_by_guess.items():
        total = 0
        for pool in pools.values():
            total += pool ** 2
        avg = total / len(pools)
        if avg < best_score:
            best_score = avg
            best = guess
            print(f"{guess} has average pool size {avg}")
    return best

    # For each hypothesis, for each guess, how many options are ruled out?
    # This basically means, for each pair (G, H), how many other values (G_i, H) match the same grade?
    # For each guess, we should find the average value of the pools that it's in.


def brute_force_solver(words, secret_grader=None):
    still_valid = words[:]
    total_guesses = 0
    print(f"Starting with {len(words)} options")
    while True:
        # 1. Pick a random word

        if len(still_valid) <= 1000:
            guess = pick_best(still_valid,
                              still_valid + sample(words, 1000))  # Allow picking from some additional words
        elif len(still_valid) > 5000:
            guess = choice(['48-32=16', '43-26=17'])  # Some starter words that profile well in dog fooding
        else:
            guess = choice(still_valid)
        if secret_grader is None:
            print("Try the following equation: ", guess)
            print("What grade did you get? Input as 8 digits, where 0=miss, 1=partial, 2=correct: ")
            secret_grade = tuple(int(c) for c in input())
            if len(secret_grade) != 8:
                print("That doesn't look right...")
                sys.exit(0)
            print(fmt(secret_grade))
        else:
            secret_grade = secret_grader(guess)
        still_valid = [w for w in still_valid if grade(guess, w) == secret_grade]
        print(
            f"#{total_guesses + 1}: Guessed: {guess} Got: {fmt(secret_grade)} Still have: {len(still_valid)} possible "
            f"options")
        total_guesses += 1
        if secret_grade == ALL_RIGHT:
            print("We won!")
            break
    return total_guesses


def estimate_good_starting_words(words, pool_size=5000, trials=2000):
    pool = sample(words, pool_size)
    start = sample(words, trials)
    return pick_best(pool, start)


def interactive(words):
    print("Hello, I'm going to help you guess your Nerdle.")
    return brute_force_solver(words)


def heuristic_one(words, secret_grader):
    pool = words[:]
    total_guesses = 1
    while len(pool) > 1:
        guess = choice(pool)
        score = secret_grader(guess)
        if score == ALL_RIGHT:
            break
        pool = [w for w in pool if grade(guess, w) == score]
        total_guesses += 1

    return total_guesses


def sim_harness(words, solver, trials):
    counts = Counter()
    tot = 0
    for i in range(trials):
        print(f"Sim: #{i + 1} of {trials}")
        ans = choice(words)
        c = solver(words, make_grader(ans))
        counts[c] += 1
        tot += c
    return tot / len(counts), max(counts.values()), min(counts.values()), counts


if __name__ == '__main__':
    seed('landslide')

    words = load_dictionary()
    print(len(words))
    test_grades()
    interactive(words)
    # avg, hi, lo, hist = sim_harness(words, heuristic_one, 1000)
    # avg, hi, lo, hist = sim_harness(words, brute_force_solver, 1000)
    # for a, b in sorted(hist.items()):
    #     print(f"{a}\t{b}")

    # estimate_good_starting_words(words, 2000, 5000)
    # pass
    # to_guess = choice(words)
    # to_guess = '11-4-2=5'
    # print("It should guess: ", to_guess)
    # brute_force_solver(words, make_grader(to_guess))
