# Nerdle Solver

For https://nerdlegame.com/.

Run it like `python3 nerdle.py` (make sure `words.txt`
is in your directory) and follow the prompts!

## Generate the Dictionary

The solver requires a dictionary of the 17,723 valid Nerdle words, which is provided as `words.txt`. You can generate
this dictionary yourself using either `gen_dictionary.py` or `fast_gen_dictionary.cpp`. The Python version takes about 8
seconds on my 2020 MacBook Pro. The C++ version takes about 5 seconds.
(Yes, this is a somewhat underwhelming speed up for a Python -> C++ port. Let's just say my C++ is kind of rusty.)

Python:

```bash
python3 gen_dictionary.py > words.txt
```

C++:

```bash
g++ fast_gen_dictionary.cpp -o gen_dictionary;
./gen_dictionary > words.txt
```