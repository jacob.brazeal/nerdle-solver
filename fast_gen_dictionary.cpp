#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

map<int, vector<string>> cache;
string nums = "0123456789";
string ops = "-+/*";
string prefix = nums + ops;

vector<string> split(string s) {
    vector<string> chunks;
    for ( auto c: s) {
        string chunk;
         chunk += c;
        chunks.push_back(chunk);
    }
    return chunks;
}

bool has_lonely_zeros(string x, bool allow_leading_zeros=true, bool allow_early_terminate=true) {
    if ( !x.size()) return false;
    if ( allow_leading_zeros && x[0] == '0' && allow_early_terminate) {
        // While building strings recursively, we need to speculatively
        // allow one or more leading zeros, in case thy are eventually
        // prefixed by a non-zero integer
        return false;
    }
    bool has_seen_zero = false;
    bool has_seen_gt_zero = false;
    for ( auto c: x ) {
        if ( c == '0' ) {
            // Zero should always follow a non-zero integer
            if ( !has_seen_gt_zero ) {
                return true;
            }
            has_seen_zero = true;
        } else if ( c >= '1' && c <= '9') {
            // A non-zero integer.
            if ( allow_early_terminate ) {
                return false;
            }
            has_seen_gt_zero = true;
        } else {
            // An operator
        }
    }
    return false;
}
vector<string> lhs(int cnt) {
    if (cache.count(cnt)){
        return cache[cnt];
    }

    if (cnt == 1) {
        return split(nums);  // Must end with a number
    }
    vector<string> exprs;

    vector<string> nxt = lhs(cnt - 1);

    for (auto val : prefix) {
        for (auto n : nxt) {
            if (ops.find(val) != string::npos && ops.find(n[0]) != string::npos)  // rule out consecutive operators
                continue;
            string expr = val + n;
            if (has_lonely_zeros(expr))
                continue;

            exprs.push_back(expr);
        }
    }

    cache[cnt] = exprs;
    return cache[cnt];
}

pair<int, bool> op(int a, int b, char o){
    if (o == '*')
        return make_pair(a * b, true);
    else if (o == '+')
        return make_pair(a + b, true);
    else if (o == '-')
        return make_pair(a - b, true);
    else if (o == '/')
        return make_pair(a / b, a % b == 0);
    return make_pair(0, false);
}

pair<int, bool> op2(int a, int b, int c, char o1, char o2){
    switch ( o1 ) {
        case '+':
            switch ( o2 ) {
                case '+':
                    return make_pair(a + b + c, true);
                case '-':
                    return make_pair(a + b - c, true);
                case '*':
                    return make_pair(a + b * c, true);
                case '/':
                    if ( c == 0 ) return make_pair(0, false);
                    return make_pair(a + b / c, b % c == 0);
            }
        case '-':
            switch ( o2 ) {
                case '+':
                    return make_pair(a - b + c, true);
                case '-':
                    return make_pair(a - b - c, true);
                case '*':
                    return make_pair(a - b * c, true);
                case '/':
                    if ( c == 0 ) return make_pair(0, false);
                    return make_pair(a - b / c, b % c == 0);
            }
        case '*':
            switch ( o2 ) {
                case '+':
                    return make_pair(a * b + c, true);
                case '-':
                    return make_pair(a * b - c, true);
                case '*':
                    return make_pair(a * b * c, true);
                case '/':
                    if ( c == 0) return make_pair(0, false);
                    return make_pair(a * b / c, (a * b) % c == 0);
            }
        case '/':
            switch ( o2 ) {
                case '+':
                    return make_pair(a / b + c, a % b == 0);
                case '-':
                    return make_pair(a / b - c, a % b == 0);
                case '*':
                    return make_pair((a * c) / b, (a * c) % b == 0);
                case '/':
                    if ( b * c == 0) return make_pair(0, false);
                    return make_pair(a / (b * c), a % (b * c) == 0);
            }
    }
    return make_pair(0, false);
}


pair<int, bool> parse_expr(string s) {
    vector<int> nums;
    vector<char> ops;
    int n = 0;
    for (auto c : s) {
        if ( c >= '0' && c <= '9') {
            n = n * 10 + (c - '0');
        } else {
            if (n) {
                nums.push_back(n);
                n = 0;
            }
            ops.push_back(c);
        }
    }
    if (n)
        nums.push_back(n);
    if (!ops.size())
        return make_pair(nums[0], true);
    else if (ops.size() == 1)
        return op(nums[0], nums[1], ops[0]);
    else { // 2 ops
        return op2(nums[0], nums[1], nums[2], ops[0], ops[1]);
    }
}

vector<string> gen_dictionary() {
    vector<string> words;
    for ( int i = 1; i < 7; ++i ) {
        for (auto e : lhs(i)) {
            if ( e[0] < '0' || e[0] > '9')
                continue;
            if (has_lonely_zeros(e, false))
                continue;
            auto val = parse_expr(e);
            if (val.second && val.first >= 0) {
                string word = e + '=' + to_string(val.first);
                if (word.size() == 8)
                    words.push_back(word);
            }
        }
    }
    return words;

}
int main() {
    auto words = gen_dictionary();
    sort(words.begin(), words.end());

    for ( auto w: words)
        cout << w << endl;
}